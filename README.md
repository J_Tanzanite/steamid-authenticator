# SteamID Authenticator

This simple sourcemod plugin fixes an exploit where players can't get banned.

I decided to release this since people for some reason where having problems solving this issue, when it's really simple.

Most plugins I've seen that try to fix this issue, kick after X amount of seconds, which is ineffective, as the cheaters can simply reconnect... again... SLIGHTLY more often.

This plugin solves the issue by blocking all usercmds (movement, actions, shooting etc) and chat from un-authenticated players.

Anyway, this should help admins and server owners not having to manually deal with hackers raiding servers and not being able to ban them.

With this, you won't have to baby sit your server making sure they don't reconnect after getting manually kicked.


# ConVars:
sidauth_enabled - Default: 1 - Enables and disables this plugin, enabeling it will block non-authenticated players from moving and chatting. 1 = on, 0 = off.

sidauth_notify - Default: 1 - Notify players when they are getting/have been authenticated. 1 = Notify in chat, 0 = off (silent).


# NOTE:

This plugin goes under the GPLv3 license.

# Compatible games: All source games.